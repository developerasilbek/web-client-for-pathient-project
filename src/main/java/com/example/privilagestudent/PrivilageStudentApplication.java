package com.example.privilagestudent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrivilageStudentApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrivilageStudentApplication.class, args);
    }

}
