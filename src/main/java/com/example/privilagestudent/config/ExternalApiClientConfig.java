package com.example.privilagestudent.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;


@Configuration
public class ExternalApiClientConfig {

    @Bean
    public WebClient webClient() {
        return WebClient.builder()
                .baseUrl("http://localhost:8080")
                .defaultHeaders(httpHeaders -> httpHeaders.setBearerAuth("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiYXV0aG9yaXRpZXMiOlt7ImlkIjozLCJodXF1cU5hbWUiOiJFRElUX1BBVElFTlQiLCJhdXRob3JpdHkiOiJFRElUX1BBVElFTlQifSx7ImlkIjoxLCJodXF1cU5hbWUiOiJBRERfUEFUSUVOVCIsImF1dGhvcml0eSI6IkFERF9QQVRJRU5UIn0seyJpZCI6MSwicm9sZU5hbWUiOiJST0xFX0FETUlOIiwiYXV0aG9yaXR5IjoiUk9MRV9BRE1JTiJ9LHsiaWQiOjQsImh1cXVxTmFtZSI6IkdFVF9QQVRJRU5UIiwiYXV0aG9yaXR5IjoiR0VUX1BBVElFTlQifSx7ImlkIjoyLCJodXF1cU5hbWUiOiJERUxFVEVfUEFUSUVOVCIsImF1dGhvcml0eSI6IkRFTEVURV9QQVRJRU5UIn0seyJpZCI6Miwicm9sZU5hbWUiOiJST0xFX0RJUkVDVE9SIiwiYXV0aG9yaXR5IjoiUk9MRV9ESVJFQ1RPUiJ9XSwiaWF0IjoxNjY2OTUxMTAyLCJleHAiOjE2Njc5NTExMDJ9.Cs1imMIh4BcL256dXMn5KbsD1yHHn8Fs3t68IdIw45_dAzfeW-MjL0alb7y6b4sgcT2VfkGFxbUo1-ERZkzszw"))
                .build();
    }





}
