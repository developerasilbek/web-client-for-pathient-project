package com.example.privilagestudent.controller;

import com.example.privilagestudent.entity.PrivilegeStudent;
import com.example.privilagestudent.payload.PageSupport;
import com.example.privilagestudent.repo.PriviligeRepo;
import com.example.privilagestudent.service.PrivilegeStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static com.example.privilagestudent.payload.PageSupport.DEFAULT_PAGE_SIZE;
import static com.example.privilagestudent.payload.PageSupport.FIRST_PAGE_NUM;

@RestController
@RequestMapping("/api/privilige")
public class PrivilegeStudentController {

    @Autowired
    PrivilegeStudentService privilegeStudentService;

    @Autowired
    PriviligeRepo priviligeRepo;



    @GetMapping("/{pinfl}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<ResponseEntity<PrivilegeStudent>> getPrivilegeById(@PathVariable Long pinfl) {
        Mono<PrivilegeStudent> privilegeStudentMono = privilegeStudentService.findById(pinfl);
        return privilegeStudentMono.map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Flux<PrivilegeStudent> getAllPrivileges() {
        return privilegeStudentService.getAllPrivileges();
    }


    @PostMapping("/{pinfl}")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<PrivilegeStudent> create(@PathVariable Long pinfl) {
        return privilegeStudentService.save(pinfl);

    }

    @PutMapping("/{pinfl}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<ResponseEntity<PrivilegeStudent>> updateUserById(@PathVariable Long pinfl, @RequestBody PrivilegeStudent privilegeStudent) {
        return privilegeStudentService.updateUser(pinfl, privilegeStudent)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.badRequest().build());
    }

    @DeleteMapping("/{pinfl}")
    @ResponseStatus(HttpStatus.OK)
    public Mono<ResponseEntity<Void>> deleteUserById(@PathVariable Long pinfl) {
        return privilegeStudentService.deletePrivilege(pinfl)
                .map(r -> ResponseEntity.ok().<Void>build())
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping("/pages")
    public Mono<PageSupport<PrivilegeStudent>> getEntitiesPage(
            @RequestParam(name = "page", defaultValue = FIRST_PAGE_NUM) int page,
            @RequestParam(name = "size", defaultValue = DEFAULT_PAGE_SIZE) int size
    ) {

        return privilegeStudentService.getEntityPage(PageRequest.of(page, size));
    }

}
