package com.example.privilagestudent.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Table
@Builder
public class PrivilegeStudent {

    @Id
    private Integer id;
    private String name;
    private String illness;
    private Long pinfl;

    public PrivilegeStudent(String name, String illness, Long pinfl) {
        this.name = name;
        this.illness = illness;
        this.pinfl = pinfl;
    }
}
