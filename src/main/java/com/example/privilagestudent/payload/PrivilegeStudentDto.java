package com.example.privilagestudent.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PrivilegeStudentDto {
    private Integer id;
    private String name;
    private String illness;
    private Long pinfl;

    public PrivilegeStudentDto(String name, String illness, Long pinfl) {
        this.name = name;
        this.illness = illness;
        this.pinfl = pinfl;
    }
}
