package com.example.privilagestudent.repo;

import com.example.privilagestudent.entity.PrivilegeStudent;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface PriviligeRepo extends ReactiveCrudRepository<PrivilegeStudent, Integer> {
    Mono<PrivilegeStudent> findByPinfl(Long pinfl);
}
