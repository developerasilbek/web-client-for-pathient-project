package com.example.privilagestudent.service;

import com.example.privilagestudent.entity.PrivilegeStudent;
import com.example.privilagestudent.payload.PageSupport;
import com.example.privilagestudent.payload.PrivilegeStudentDto;
import com.example.privilagestudent.repo.PriviligeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.stream.Collectors;

@Service
public class PrivilegeStudentService {

    private final WebClient webClient;

    public PrivilegeStudentService(WebClient webClient) {
        this.webClient = webClient;
    }

    @Autowired
    PriviligeRepo priviligeRepo;

    public Mono<PrivilegeStudentDto> findPrivilegeById(Long pinfl) {
        return webClient.get()
                .uri(uriBuilder -> uriBuilder.path("/api/patient/{pinfl}")
                        .build(pinfl))
                .retrieve()
                .bodyToMono(PrivilegeStudentDto.class);
    }

    public Mono<PrivilegeStudent> findById(Long pinfl) {
        return priviligeRepo.findByPinfl(pinfl);
    }

    public Flux<PrivilegeStudent> getAllPrivileges() {
        return priviligeRepo.findAll();
    }


    public Mono<PrivilegeStudent> save(Long pinfl) {
        return findPrivilegeById(pinfl)
                .flatMap(st -> priviligeRepo.save(PrivilegeStudent.builder()
                        .name(st.getName())
                        .illness(st.getIllness())
                        .pinfl(st.getPinfl())
                        .build()))
                .map(dto -> PrivilegeStudent.builder()
                        .id(dto.getId())
                        .illness(dto.getIllness())
                        .name(dto.getName())
                        .pinfl(dto.getPinfl())
                        .build());
    }

    public Mono<PrivilegeStudent> updateUser(Long pinfl, PrivilegeStudent privilegeStudent) {
        return priviligeRepo.findByPinfl(pinfl)
                .flatMap(c -> {
                    c.setIllness(privilegeStudent.getIllness());
                    c.setName(privilegeStudent.getName());
                    c.setPinfl(privilegeStudent.getPinfl());
                    return priviligeRepo.save(c);
                });
    }


    public Mono<PrivilegeStudent> deletePrivilege(Long pinfl) {
        return priviligeRepo.findByPinfl(pinfl)
                .flatMap(existingPrivilege -> priviligeRepo.delete(existingPrivilege)
                        .then(Mono.just(existingPrivilege)));
    }

    public Mono<PageSupport<PrivilegeStudent>> getEntityPage(Pageable page) {
        return priviligeRepo.findAll()
                .collectList()
                .map(list -> new PageSupport<>(
                        list
                                .stream()
                                .skip((long) page.getPageNumber() * page.getPageSize())
                                .limit(page.getPageSize())
                                .collect(Collectors.toList()),
                        page.getPageNumber(), page.getPageSize(), list.size()));
    }


}

